package com.qa.page;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.util.GenericFunction;

public class LoginPage extends GenericFunction{
	//Initializing the Page Objects:
	public LoginPage() {
		super();
		PageFactory.initElements(driver, this);
	}
	
	//Page Factory - OR:
	@FindBy(xpath="//input[@id='user-name']")
	public WebElement txtUserID;
	
	@FindBy(css="input#password")
	public WebElement txtPassword;
	
	@FindBy(xpath="//button[@id='sign-on']") 
	public WebElement btnSignOn;
}
