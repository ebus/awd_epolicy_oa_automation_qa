package com.qa.page;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.util.GenericFunction;

public class HomePage extends GenericFunction{
	//Initializing the Page Objects:
	public HomePage() {
		super();
		PageFactory.initElements(driver, this);
	}
	
	//Page Factory - OR:
	
	@FindBy(xpath = "//input[@title='Policy Number']") 
	public WebElement policynumberfield;
	
	@FindBy(xpath = "//button[@id='Search_1_']") 
	public WebElement searchbutton;
	
	@FindBy(xpath = "//img[@id='undefined_img' and @alt='Add Create']") 
	public WebElement optionCreate;
	
	@FindBy(xpath = "//img[@id='undefined_imghover' and @alt='Add Search']") 
	public WebElement optionSearch;
	
	@FindBy(xpath = "//div[@class='dstSearchResults']/descendant::div[@class='dstDecorator']/child::div[2]") 
	public WebElement policyexpand;
	
	@FindBy(xpath = "//div[@class='dstSearchResults']/descendant::div[@class='dstDecorator']/child::div[@class='awdObjectAction ']") 
	public WebElement worktypexpand;
	
	@FindBy(xpath = "//div[@class='dstSearchResults']/descendant::div[@class='dstDecorator']/child::div[@class='awdObjectAction '][1]/descendant::ul/child::li[@id='awd_menu_li_actnopen']/child::a[contains(text(),Open)]")
	public WebElement openbutton;
			
	@FindBy(xpath = "//ul[@class='dstListTree awdObjects']/child::li[@class='awdObject']/child::div[@class ='dstWorkList dstResultsList']/child::ul[@class='dstListTree awdObjects']/child::li") 
	public WebElement attachmentpath;
		
	@FindBy(xpath = "//a[contains(text(), 'POLN/INSL/WORKTYPE')]") 
	public WebElement searchbypolicynumber;

	@FindBy(xpath = "//a[contains(text(), 'Create Work')]") 
	public WebElement btnCreateWork;
	
	@FindBy(xpath = "//a[contains(text(), 'Create Folder')]") 
	public WebElement btnCreateFolder;
	
	@FindBy(xpath = "//select[@awdname = 'status']") 
	public WebElement statuscode;
	
	@FindBy(xpath = "//label[@name='AGTN']/child::span[text()='Agent Number']/following-sibling::input")
	public WebElement agent_number;
	
	@FindBy(xpath = "//label[@name='AGTL']/child::span/following-sibling::input")
	public WebElement agent_lastname;
	
	@FindBy(xpath = "//label[@name='AGTF']/child::span/following-sibling::input")
	public WebElement agent_firstname;
	
	@FindBy(xpath = "//span[text() = 'ePolicy Owner']/following-sibling::input")
	public WebElement epolicy_owner;
	
	@FindBy(xpath = "//span[text() = 'Insured Guardian']/following-sibling::input")
	public WebElement insured_guardian;
	
	@FindBy(xpath = "//span[text() = 'Co-Owner 1 First/Last Name']/following-sibling::input")
	public WebElement co_owner1_name;
	
	@FindBy(xpath = "//span[text() = 'Co-Own 1 Last 4 SSN']/following-sibling::input")
	public WebElement co_owner1_ssn;
	
	@FindBy(xpath = "//span[text() = 'Co-Owner 2 First/Last Name']/following-sibling::input")
	public WebElement co_owner2_name;
	
	@FindBy(xpath = "//span[text() = 'Co-Own 2 Last 4 SSN']/following-sibling::input")
	public WebElement co_owner2_ssn;
	
	@FindBy(xpath = "//span[text() = 'Balance of Initial Premium Due']/following-sibling::input")
	public WebElement initialpremiumDue;
	
	@FindBy(xpath = "//label[@name='status']/following-sibling::button[contains(text(),'Update')]")
	public WebElement updatebutton;

	
	@FindBy(css = "#awd_logoff_link") 				
	public WebElement btnSignOff;
}
