package com.qa.pageactions;

import java.awt.AWTException;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.qa.page.HomePage;


public class HomePageAction extends HomePage{
	public HomePageAction() {
		super();
	}
	static WebElement btnUnderFolder_Work_1;
	static WebElement btnUnderFolder_Work_2;
	static WebElement btnUnderFolder_Work_3;
	static WebElement btnFolder;
	//Actions:	

	
	public void clickCreateWork() throws InterruptedException {
//		extentTest.log(LogStatus.INFO, "-----Home Page-----");
		ClickJSElement(optionCreate, "Create Button");
		ClickJSElement(btnCreateWork, "CreateWork Button");
		Thread.sleep(3000);
	}
	
	public void clickCreateFolder() throws InterruptedException {
//		extentTest.log(LogStatus.INFO, "-----Home Page-----");
		ClickJSElement(optionCreate, "Create Folder");
		ClickJSElement(btnCreateFolder, "CreateWork Folder");
		Thread.sleep(3000);
	}
	
	public void clickSearchButton() throws InterruptedException {
//		extentTest.log(LogStatus.INFO, "-----Home Page-----");
		ClickJSElement(optionSearch, "Search Option");
		ClickJSElement(searchbypolicynumber, "search by policy number");
		Thread.sleep(3000);
	}
	
	public void searchbypolicynumber(String policynumber) throws InterruptedException {
		EnterText(policynumberfield, policynumber, "Policy Number");
		ClickJSElement(searchbutton, "Click on search Button");
		Thread.sleep(3000);
	}
	
	public void openpolicydetail(String StatusUpdate_1, String AgentDetailUpdate, 
			String AgentNumber, String AgentLastName, String AgentFirstName, String epolicyinfoUpdate, String epolicyOwner, String InsuredGuardianUpdate,String InsuredGuardian, String co_owner1Update,
			String co_owner1name, String co_owner1SSN, String co_owner2Update,String co_owner2name, String co_owner2SSN, String initialpremiumDueUpdate,String initial_premium_Due) throws InterruptedException, AWTException {
		driver.manage().timeouts().pageLoadTimeout(30,TimeUnit.SECONDS);
		WebDriverWait wait = new WebDriverWait(driver,30);
		Actions act = new Actions(driver);
		if(policyexpand.getAttribute("class").contains("Hidden")) {
			
		}
		else {	
			act.moveToElement(worktypexpand).build().perform();
			Thread.sleep(2000);
			ClickJSElement(worktypexpand, "Click on open button");
//			worktypexpand.click();
			Thread.sleep(2000);
			ClickJSElement(openbutton, "Click on open button");
			Thread.sleep(25000);
			Robot r = new Robot();
			r.keyPress(KeyEvent.VK_ALT);
			r.keyPress(KeyEvent.VK_F4);
			r.keyRelease(KeyEvent.VK_F4);
			r.keyRelease(KeyEvent.VK_ALT);
			ClickJSElement(policyexpand, "Click on policy expand button button");
//			policyexpand.click();
			List<WebElement> list = driver.findElements(By.xpath("//div[@class = 'dstModule dstCollapsible dstCloseable dstConfigurable dstRefreshable dstDecorated userScreenBodyStyle']/descendant::div[@class='dstWorkList dstResultsList']/child::ul/child::li/child::div[@class='dstWorkList dstResultsList']/child::ul/child::li"));
			int size = list.size();
				for(int i=1;i<=size;i++)
				{
					String beforeXpath = "//div[@class = 'dstModule dstCollapsible dstCloseable dstConfigurable dstRefreshable dstDecorated userScreenBodyStyle']/descendant::div[@class='dstWorkList dstResultsList']/child::ul/child::li/child::div[@class='dstWorkList dstResultsList']/child::ul/child::li[";
					String afterXpath_attachmentclick = "]/descendant::div[@class='awdObjectAction ']/child::img";
					String afterXpath_attachmentopen = "]/descendant::ul/child::li/child::a[contains(text(),'Open')]";
					String actualxpath_attachmentclick=beforeXpath+i+afterXpath_attachmentclick;
					String actualxpath_attachmentopen=beforeXpath+i+afterXpath_attachmentopen;
					ClickJSElement(driver.findElement(By.xpath(actualxpath_attachmentclick)),"attachmentclickbutton");
					ClickJSElement(driver.findElement(By.xpath(actualxpath_attachmentopen)),"attachmentopenbutton");
					Thread.sleep(5000);
					r.keyPress(KeyEvent.VK_ALT);
					r.keyPress(KeyEvent.VK_F4);
					r.keyRelease(KeyEvent.VK_F4);
					r.keyRelease(KeyEvent.VK_ALT);			
				}
			}
		act.moveToElement(worktypexpand).build().perform();
		Thread.sleep(2000);
		ClickJSElement(worktypexpand, "Click on open button");
//		worktypexpand.click();
		Thread.sleep(2000);
		ClickJSElement(openbutton, "Click on open button");
		wait.until(ExpectedConditions.visibilityOf(statuscode));
		ComboSelectValue(statuscode,StatusUpdate_1,"Status code updated");
		if(AgentDetailUpdate.equalsIgnoreCase("Yes")) {
			EnterText(agent_number, AgentNumber, "agent number");
			EnterText(agent_lastname, AgentLastName, "agent last name");
			EnterText(agent_firstname, AgentFirstName, "agent first name");
		}
			
		if(InsuredGuardianUpdate.equalsIgnoreCase("Yes")) {
			EnterText(epolicy_owner, epolicyOwner, "epolicy owner name");
		}
		if(epolicyinfoUpdate.equalsIgnoreCase("Yes")) {
			EnterText(insured_guardian, InsuredGuardian, "insured guardian name");
		}
		if(co_owner1Update.equalsIgnoreCase("Yes")) {
			EnterText(co_owner1_name, co_owner1name, "co_owner1 name");
			EnterText(co_owner1_ssn, co_owner1SSN, "co_owner1 ssn details");
		}
		if(co_owner2Update.equalsIgnoreCase("Yes")) {
			EnterText(co_owner2_name, co_owner2name, "co_owner2 name");
			EnterText(co_owner2_ssn, co_owner1SSN, "co_owner2 ssn details");
			}
		if(initialpremiumDueUpdate.equalsIgnoreCase("Yes")) {		
			EnterText(initialpremiumDue, initial_premium_Due, "Balance of initial premium Due");
		}
		ClickJSElement(updatebutton,"click on update button");	
		Thread.sleep(2000);
	}
	
	public void openpolicydetailforstatusupdate(String StatusUpdate_2) throws InterruptedException, AWTException {
		Actions act = new Actions(driver);
		WebDriverWait wait = new WebDriverWait(driver,30);
		if(policyexpand.getAttribute("class").contains("Hidden")) {
			act.moveToElement(worktypexpand).build().perform();
			Thread.sleep(2000);
			ClickJSElement(worktypexpand, "Click on open button");
//			worktypexpand.click();
			Thread.sleep(2000);
			ClickJSElement(openbutton, "Click on open button");
			Thread.sleep(3000);
		}
		else {	
			act.moveToElement(worktypexpand).build().perform();
			Thread.sleep(2000);
			ClickJSElement(worktypexpand, "Click on open button");
//			worktypexpand.click();
			Thread.sleep(2000);
			ClickJSElement(openbutton, "Click on open button");
			Thread.sleep(8000);
			Robot r = new Robot();
			r.keyPress(KeyEvent.VK_ALT);
			r.keyPress(KeyEvent.VK_F4);
			r.keyRelease(KeyEvent.VK_F4);
			r.keyRelease(KeyEvent.VK_ALT);
		}
		ComboSelectValue(statuscode,StatusUpdate_2,"Status code updated");
	    wait.until(ExpectedConditions.elementToBeClickable(updatebutton));
		ClickJSElement(updatebutton,"click on update button");	
		Thread.sleep(2000);
	   }

	public void signOff() throws InterruptedException, AWTException {
		Thread.sleep(2000);
		ClickJSElement(btnSignOff, "Sign Off");
		Thread.sleep(3000);
		Robot r = new Robot();
		r.keyPress(KeyEvent.VK_ENTER);
		r.keyRelease(KeyEvent.VK_ENTER);
	}
	
}
