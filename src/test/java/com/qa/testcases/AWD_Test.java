package com.qa.testcases;

import java.awt.AWTException;
import java.io.IOException;

import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.qa.base.TestBase;
import com.qa.pageactions.LoginPageAction;
import com.qa.pageactions.HomePageAction;
import com.qa.util.TestUtil;
import com.relevantcodes.extentreports.LogStatus;

public class AWD_Test extends TestBase{
	public AWD_Test() {
		super();
	}
	
	LoginPageAction loginPageAction;
	HomePageAction homePageAction;
	
	@DataProvider
	public Object[][] getCOTestData(){
		Object data[][] = TestUtil.getTestData("epolicy");
		return data;
	}
	
	@Test(dataProvider="getCOTestData")
	public void applicationAWD_test(String scenarioName, String indExecute, String policynumber, String StatusUpdate_1, String AgentDetailUpdate, 
			String AgentNumber, String AgentLastName, String AgentFirstName, String epolicyinfoUpdate, String epolicyOwner, String InsuredGuardianUpdate,String InsuredGuardian, String co_owner1Update,
			String co_owner1_name, String co_owner1_SSN, String co_owner2Update, String co_owner2_name, String co_owner2_SSN, String initialpremiumDueUpdate,String initialpremiumDue,
			String StatusUpdate_2, String StatusUpdate_3) throws IOException, InterruptedException, AWTException {
		
		extentTest = extent.startTest("AWD " + scenarioName);
		if (indExecute.equalsIgnoreCase("No")) {
			extentTest.log(LogStatus.INFO, "Execute column is No in the data sheet");
	        throw new SkipException(scenarioName + " is Skipped");
	    }
		Thread.sleep(3000);
		initialization();
		extentTest.log(LogStatus.INFO, "URL: " + prop.getProperty("url"));
		Thread.sleep(3000);
		loginPageAction = new LoginPageAction();
		homePageAction = loginPageAction.login(prop.getProperty("loginID"), prop.getProperty("password"));
		homePageAction.clickSearchButton();
		homePageAction.searchbypolicynumber(policynumber);
		homePageAction.openpolicydetail(StatusUpdate_1, AgentDetailUpdate, AgentNumber, AgentLastName, AgentFirstName, epolicyinfoUpdate, epolicyOwner, 
				InsuredGuardianUpdate,InsuredGuardian, co_owner1Update,co_owner1_name, co_owner1_SSN,co_owner2Update, co_owner2_name, co_owner2_SSN, initialpremiumDueUpdate,initialpremiumDue);
		extentTest.log(LogStatus.INFO, extentTest.addScreenCapture(TestUtil.passedScreenshotForReport()));
		homePageAction.openpolicydetailforstatusupdate(StatusUpdate_2);
		extentTest.log(LogStatus.INFO, extentTest.addScreenCapture(TestUtil.passedScreenshotForReport()));
		homePageAction.openpolicydetailforstatusupdate(StatusUpdate_3);
		homePageAction.signOff();
		extentTest.log(LogStatus.PASS, extentTest.addScreenCapture(TestUtil.passedScreenshotForReport()));
	}	
}
